const request = require('supertest')
const { User } = require('../users/users.model')
const { Plant } = require('../plants/plants.model')

describe('api/auth', () => {
    let server
    beforeEach(() => server = require('../../server'))
    afterEach(async () => {
        await server.close()
        await Plant.remove({})
        await User.remove({})
    })

    describe('auth middleware', () => {
        let token
        const exec = async () => {
            return await request(server)
                .post('/api/plants')
                .set('Authorization', token)
                .send({plant_name: 'Plant1'})
        }
        beforeEach(() => {
            const newUser = new User({ name: "Sergio", email: "giorgiosaints@gmail.com", password: "1234567", is_admin: true })
            token = newUser.generateAuthToken()
        })

        it('should return 401 if no token is provided', async () => {
            token = ''
            const res = await exec()
            expect(res.status).toBe(401)
        })

        it('should return 400 if token is invalid', async () => {
            token = 'a'
            const res = await exec()
            expect(res.status).toBe(400)
        })

        it('should return 200 if token is valid', async () => {
            const res = await exec()
            expect(res.status).toBe(200)
        })
    })

    describe('POST: /api/auth/sign_up', () => {
        let user
        const exec = () => {
            return request(server)
                .post('/api/auth/sign_up')
                .send({ name: user.name, email: user.email, password: user.password })
        }

        beforeEach(async () => {
            user = { name: "Sergio", email: "teste@gmail.com", password: "1234567" }
        })

        it('should return 400 if name is not provided', async () => {
            user.name = ''

            const res = await exec() 
            
            expect(res.status).toBe(400)
        })

        it('should return 400 if email is not provided', async () => {
            user.email = ''

            const res = await exec() 
            
            expect(res.status).toBe(400)
        })

        it('should return 400 if password is not provided', async () => {
            user.password = ''

            const res = await exec() 
            
            expect(res.status).toBe(400)
        })

        it('should return 400 if user already registered', async() => {
            const newUser = new User(user)
            await newUser.save()

            const res = await exec()

            expect(res.status).toBe(400)
        })

        it('should return 200 if user is valid request', async () => {
            const res = await exec() 
            
            expect(res.status).toBe(200)
        })

        it('should return user if is valid request', async () => {
            const res = await exec() 
            
            expect(Object.keys(res.body)).toEqual(
                expect.arrayContaining(['_id', 'name', 'email'])
            )
        })
    })

    describe('POST: /api/auth/sign_in', () => {
        let user_login
        const exec = () => {
            return request(server)
                .post('/api/auth/sign_in')
                .send({email: user_login.email, password: user_login.password})
        }
        
        beforeEach(async () => {
            user_login = { name: "Sergio", email: "teste@gmail.com", password: "1234567" }
            const newUser = new User(user_login)

            await newUser.save()
        })

        it('should return 400 if email is not provided', async () => {
            user_login.email = ''

            const res = await exec()

            expect(res.status).toBe(400)
        })

        it('should return 400 if password is not provided', async () => {
            user_login.password = ''

            const res = await exec()

            expect(res.status).toBe(400)
        })

        it('should return 400 if password not matches', async () => {
            user_login.password = '1234'

            const res = await exec()

            expect(res.status).toBe(400)
        })

        it('should return 200 if user is valid request', async () => {
            const res = await exec()

            expect(res.status).toBe(200)
        })
        
        it('should return user if is valid request', async () => {
            const res = await exec()

            expect(Object.keys(res.body)).toEqual(
                expect.arrayContaining(['name', 'email', 'accessToken'])
            )
        })
    })

})