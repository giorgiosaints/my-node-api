const debug = require('debug')('controller:auth')
const _ = require('lodash')
const { User, validate, validate_user_auth } = require('../users/users.model')

exports.sign_up = async (req, res) => {
    try {
        const { error } = validate(req.body)
        if (error) return res.status(400).send(error.details[0].message)

        let user = await User.findByEmail(req.body.email)
        if (user) return res.status(400).send('User already registered.')

        user = new User(_.pick(req.body, ['name', 'email', 'password']))
        await user.save()

        const token = user.generateAuthToken()
        res.header('Authorization', token)
            .send(_.pick(user, ['_id', 'name', 'email']))
    } catch (error) {
        debug(error)
        res.json(error)
    }
}

exports.sign_in = async (req, res) => {
    try {
        const { error } = validate_user_auth(req.body)
        if (error) return res.status(400).send(error.details[0].message)

        let user = await User.findByEmail(req.body.email)
        if (!user) return res.status(400).send('Invalid email or password')

        const valid_password = await user.passwordMatches(req.body.password)
        if (!valid_password) return res.status(400).send('Invalid email or password')

        const token = user.generateAuthToken()
        res.send({name: user.name, email: user.email, accessToken: token})
    } catch (error) {
        debug(error)
        res.json(error)
    }
}