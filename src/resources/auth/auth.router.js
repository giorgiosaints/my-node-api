const express = require('express')
const router = express.Router()

const auth_controller = require('./auth.controller')

router.post('/sign_up', auth_controller.sign_up)
router.post('/sign_in', auth_controller.sign_in)

module.exports = router;