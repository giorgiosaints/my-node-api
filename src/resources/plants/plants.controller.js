const { Plant, validate } = require('./plants.model')

exports.all = async (req, res, next) => {
	const plants = await Plant
		.find()
		.populate('geofences')

	res.json(plants)
}

exports.show = async (req, res) => {
	const plant = await Plant.findById(req.params.id)
	if (!plant) return res.status(404).send('Invalid plant')

	res.json(plant)
}

exports.create = async (req, res) => {
	const { error } = validate(req.body)
	if (error) return res.status(400).send(error.details[0].message)

	const new_plant = new Plant(req.body)
	const plant = await new_plant.save()
	res.json(plant)
}

exports.update = async (req, res) => {
	const { error } = validate(req.body)
	if (error) return res.status(400).send(error.details[0].message)
	
	const options = { new: true }

	const plant = await Plant.findByIdAndUpdate(req.params.id, req.body, options)
	res.json(plant)
}

exports.delete = async (req, res) => {
	const plant = await Plant.findByIdAndRemove(req.params.id)
}