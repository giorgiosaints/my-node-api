const mongoose = require('mongoose')
const Joi = require('joi')
Joi.objectId = require('joi-objectid')(Joi)

const { geofencesSchema } = require('../geofences/geofences.model')

const plantSchema = new mongoose.Schema({
	plant_name: { 
		type: String,
		required: true,
		minlength: 5,
		maxlength: 50
	},
	geofences: {
		type: [geofencesSchema],
	},
	lat: Number,
	lng: Number
})

// TODO: Middlewares
const validate = (plant) => {
	const schema = {
		plant_name: Joi.string().min(5).max(50).required(),
		lat: Joi.number(),
		lng: Joi.number()
	}

	return Joi.validate(plant, schema)
}

const Plant = mongoose.model('Plants', plantSchema)

exports.Plant = Plant
exports.validate = validate