const auth = require('../../security/auth.middleware')
const authz = require('../../security/authz.middleware')
const validate_object_id = require('../../middlewares/validate_object_id')
const express = require('express')
const router = express.Router()

const plants_controller = require('./plants.controller')

router.get('/', plants_controller.all)
router.get('/:id', validate_object_id, plants_controller.show)
router.post('/', [auth, authz], plants_controller.create)
router.patch('/:id', auth, validate_object_id, plants_controller.update)
router.delete('/:id', [auth, authz, validate_object_id], plants_controller.delete)

module.exports = router