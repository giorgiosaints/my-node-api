const mongoose = require('mongoose')
const request = require('supertest')
const { Plant } = require('./plants.model')
const { User } = require('../users/users.model')

describe('/api/plants', () => {
    let server
    beforeEach(() => server = require('../../server'))
    afterEach(async () => {
        await server.close()
        await Plant.remove({})
    })

    describe('plantSchema', () => {
        it('method: validate_object_id', () => {
            const obj_id = mongoose.Types.ObjectId()
            const result = mongoose.Types.ObjectId.isValid(obj_id)
            expect(result).toBe(true)
        })
    })

    describe('GET /plants', () => {
        it('should return all plants', async () => {
            await Plant.collection.insertMany([
                { plant_name: "Plant 1", geofences: null, lat: 1000, lng: 2000},
                { plant_name: "Plant 2", geofences: null, lat: 1000, lng: 2000}
            ])

            const res = await request(server).get('/api/plants')
            expect(res.status).toBe(200)
            expect(res.body.length).toBe(2)
            expect(res.body.some(p => p.plant_name === 'Plant 1')).toBeTruthy()
            expect(res.body.some(p => p.plant_name === 'Plant 2')).toBeTruthy()
        })
    })

    describe('GET /plants/:id', () => {
        it('should return a plant if valid id is passed', async () => {
            const plant = new Plant({plant_name: 'Plant 1'})
            await plant.save()

            const res = await request(server).get(`/api/plants/${plant._id}`)
            expect(res.status).toBe(200)
            expect(res.body).toHaveProperty('plant_name', plant.plant_name)
        })

        it('should return 404 if invalid id is passed', async () => {
            const res = await request(server).get(`/api/plants/1`)
            expect(res.status).toBe(404)
        })
    })

    describe('POST /plants', () => {
        let newUser
        let token
        let is_admin
        let plant_name
        const exec = async () => {
            return await request(server)
                .post('/api/plants')
                .set('Authorization', token)
                .send({ plant_name })
        }

        beforeEach(() => {
            plant_name = 'Plant1'

            newUser = new User({ name: "Sergio", email: "giorgiosaints@gmail.com", password: "1234567", is_admin: true })
            token = newUser.generateAuthToken()
        })

        it('should return 401 if client is not logged in', async () => {
            token = ''
    
            const res = await exec()
            
            expect(res.status).toBe(401)
        })

        it('should return 403 if client is not authenticated', async () => {
            newUser = new User({ name: "Sergio", email: "giorgiosaints@gmail.com", password: "1234567", is_admin: false })
            token = newUser.generateAuthToken()
            
            const res = await exec()
            
            expect(res.status).toBe(403)
        })

        it('should return 400 if plant name is less than 5 characters', async () => {
            plant_name = '1234'

            const res = await exec()

            expect(res.status).toBe(400)
        })

        it('should return 400 if plant name is more than 50 characters', async () => {
            plant_name = new Array(52).join('a')

            const res = await exec()

            expect(res.status).toBe(400)
        })

        it('should save the plant if it is valid', async () => {
            await exec()

            const plant = await Plant.find({ plant_name: 'Plant1' })
            expect(plant).not.toBeNull()
        })

        it('should return the plant if it is valid', async () => {
            const res = await exec()

            expect(res.status).toBe(200)
            expect(res.body).toHaveProperty('_id')
            expect(res.body).toHaveProperty('plant_name', 'Plant1')
        })
        
        it('should return status 200 when save a plant', async () => {
            const res = await exec()

            expect(res.status).toBe(200)
        })
    })
})
