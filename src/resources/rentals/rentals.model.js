const mongoose = require('mongoose')
const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi)

const customerSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 50
    },
    isGold: {
        type: Boolean,
        default: false
    },
    phone: {
        type: String,
        require: true,
        minlength: 5,
        maxlength: 50
    }
})

const movieSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
        trim: true,
        minlength: 5,
        maxlength: 255
    },
    dailyRentalRate: {
        type: Number,
        required: true,
        min: 0,
        max: 255
    }
})

const rentalSchema = new mongoose.Schema({
    customer: {
        type: [customerSchema],
        required: true
    },
    movie: {
        type: [movieSchema],
        required: true
    },
    dateOut: {
        type: Date,
        required: true,
        default: Date.now
    },
    dateReturned: {
        type: Date
    },
    rentalFee: {
        type: Number,
        min: 0
    }
})

const validateRental = (rental) => {
    const schema = {
        customer_id: Joi.objectId().required(),
        movie_id: Joi.objectId().required()
    }

    return Joi.validate(rental, schema)
}

// Custom Static Method
rentalSchema.statics.lookup = function(customer_id, movie_id) {
    return this.findOne({
        'customer._id': customer_id,
        'movie._id': movie_id
    })
}

// Custom Instance Method
// rentalSchema.method.return = function() {
    // this.dateReturned = new Date()
    
    // const rentalDays = moment().diff(this.dateOut, 'days')
    // rental.rentalFee = rentalDays * this.movie.dailyRentalRate
// }

const Customer = mongoose.model('Customer', customerSchema)
const Movie = mongoose.model('Movie', movieSchema)
const Rental = mongoose.model('Rental', rentalSchema)

exports.Customer = Customer
exports.Movie = Movie
exports.Rental = Rental
exports.validateRental = validateRental