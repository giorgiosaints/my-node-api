const request = require('supertest')
const mongoose = require('mongoose')
const moment = require('moment')
const { Rental } = require('./rentals.model')
const { Movie } = require('./rentals.model')
const { User } = require('../users/users.model')

// TEST CASE - TDD
// POST /api/rentals
// Return 401 if client is not logged in
// Return 400 if customer_id is not provided
// Return 400 if movie_id is not provided
// Return 404 if no rental found for this customer/movie
// Return 400 if rental already processed
// Return 200 if valid request
// Set the return date
// Calculate the rental fee (numberOfDays * movie.dailyRentalRate)
// Increase the stock
// Return the rental

describe('/api/rentals', () => {
    let server
    let customer_id
    let movie_id
    let newRental
    let newMovie

    beforeEach(async () => {
        server = require('../../server')

        customer_id = mongoose.Types.ObjectId()
        movie_id = mongoose.Types.ObjectId()

        newMovie = new Movie({
            _id: movie_id,
            title: '12345',
            dailyRentalRate: 2,
            genre: { name: '12345'},
            numberInStock: 10

        })

        await newMovie.save()

        newRental = new Rental({
            customer: {
                _id: customer_id,
                name: '12345',
                phone: '12345'
            },
            movie: {
                _id: movie_id,
                title: '12345',
                dailyRentalRate: 2
            }
        })
        await newRental.save()
    })

    afterEach(async () => {
        await server.close()
        await Rental.remove({})
        await Movie.remove({})
    })

    describe('POST: /api/rentals', () => {
        let token
        const exec = () => {
            return request(server)
                .post('/api/rentals')
                .set('Authorization', token)
                .send({ customer_id, movie_id })
        }

        beforeEach(() => {
            const newUser = new User({ name: "Sergio", email: "giorgiosaints@gmail.com", password: "1234567", is_admin: true })
            token = newUser.generateAuthToken()
        })

        it('should return 401 if client is not logged in', async () => {
            token = ''

            const res = await exec()

            expect(res.status).toBe(401)
        })

        it('should return 400 if customer id is not provided', async () => {
            customer_id = ''

            const res = await exec()

            expect(res.status).toBe(400)
        })

        it('should return 400 if movie id is not provided', async () => {
            movie_id = ''

            const res = await exec()

            expect(res.status).toBe(400)
        })

        it('should return 404 if no rental found for this customer/movie', async () => {
            await Rental.remove({})

            const res = await exec()

            expect(res.status).toBe(404)
        })

        it('should return 200 if valid request', async () => {
            const res = await exec()

            expect(res.status).toBe(200)
        })

        it('should return the rental if input is valid', async () => {
            const res = await exec()

            const rentalInDb = await Rental.findById(newRental._id)
            expect(Object.keys(res.body)).toEqual(
                expect.arrayContaining(['customer', 'movie']))
        })

        // it('should return 400 if rental already processed', async () => {
        //     newRental.dateReturned = new Date()
        //     await newRental.save()

        //     const res = await exec()

        //     expect(res.status).toBe(400)
        // })

         // it('should set the returnDate if input is valid', async () => {
        //     const res = await exec()

        //     const rentalInDb = await Rental.findById(newRental._id)
        //     const diff = new Date() - rentalInDb.dateReturned
        //     expect(diff).toBeLessThan(10 * 1000)
        // })
        
        // it('should calculate the rentalFee (numberOfDays * movie.dailyRentalRate)', async () => {
        //     newRental.dateOut = moment().add(-7, 'days').toDate() // 7 days ago momentJs
        //     await newRental.save()
        
        //     const res = await exec()
        
        //     const rentalInDb = await Rental.findById(newRental._id)
        //     expect(rentalInDb.rentalFee).toBeDefined(14)
        // })

        // it('should increase the movie stock if input is valid', async () => {
        //     const res = await exec()

        //     const movieInDb = await Movie.findById(movie_id)
        //     expect(movieInDb.numberInStock).toBe(newMovie.numberInStock + 1)
        // })
    })

})