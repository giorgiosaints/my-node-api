const express = require('express')
const router = express.Router()
const auth = require('../../security/auth.middleware')
const validate = require('../../middlewares/validate')
const { validateRental } = require('./rentals.model')
const rentals_controller = require('./rentals.controller')

router.post('/', [auth, validate(validateRental)], rentals_controller.create)

module.exports = router