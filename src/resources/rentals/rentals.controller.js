const { Rental } = require('./rentals.model')

exports.create = async (req, res) => {
    const rental = await Rental.lookup(req.body.customer_id, req.body.movie_id)
    if (!rental) return res.status(404).send('Rental not found')

    // rental.return()
    // await rental.save()
    return res.send(rental)

    // await Movie.update({ _id: rental.movie._id }, {
    //     $inc: { numberInStock: 1 }
    // })

    // const new_plant = new Plant(req.body)
    // const plant = await new_plant.save()
    // res.json(plant)
}
