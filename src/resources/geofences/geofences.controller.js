const debug = require('debug')('controller:geofences')

const { Plant } = require('../plants/plants.model')
const { Geofence } = require('../geofences/geofences.model')

exports.create = async (req, res) => {
    try {
        const plant = await Plant.findById(req.params.plant_id)
        plant.geofences.push(new Geofence(req.body))
		plant.save()
		res.json(plant)
	} catch (error) {
		debug(error)
		res.send(404)
	}
}

exports.delete = async (req, res) => {
    try {
		const plant = await Plant.findById(req.params.plant_id)
		if (!plant) return res.status(400).send('Invalid plant')
        const geofence = await plant.geofences.id(req.params.id)
        geofence.remove()
        plant.save()

		res.json(plant)
	} catch (error) {
		debug(error)
		res.send(404)
	}
}