const auth = require('../../security/auth.middleware')
const express = require('express')
const router = express.Router()

const geofences_controller = require('./geofences.controller')

router.post('/:plant_id/geofences', auth, geofences_controller.create)
router.delete('/:plant_id/geofences/:id', auth, geofences_controller.delete)

module.exports = router
