const mongoose = require('mongoose')

const geofencesSchema = new mongoose.Schema({
    active: Boolean,
    coordinates: {
        type: []
    },
    radius: Number,
    type: {
        type: String,
        enum: ['polygon', 'point'],
        lowercase: true,
        default: 'polygon',
        trim: true
    }
})

const Geofence = mongoose.model('Geofences', geofencesSchema)

exports.geofencesSchema = geofencesSchema
exports.Geofence = Geofence