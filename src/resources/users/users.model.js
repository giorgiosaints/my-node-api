const mongoose = require('mongoose')
const config = require('config');
const Joi = require('joi')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 50
    },
    email: {
        type: String,
        required: true,
        unique: true,
        minlength: 5,
        maxlength: 255
    },
    password: {
        type: String,
        required: true,
        minlength: 6,
        maxlength: 1024
    },
    is_admin: {
        type: Boolean,
        default: false
    }
})

// TODO: Middlewares
const validate = (user) => {
    const schema = {
        name: Joi.string().min(5).max(50).required(),
        email: Joi.string().min(5).max(255).required().email(),
        password: Joi.string().min(6).max(255).required()
    }

    return Joi.validate(user, schema)
}

const validate_user_auth = (user) => {
    const schema = {
        email: Joi.string().min(5).max(255).required().email(),
        password: Joi.string().min(6).max(255).required()
    }

    return Joi.validate(user, schema)
}

const password_encrypt = async (obj, next) => {
    try {
        const salt = await bcrypt.genSalt(config.get('Security.saltRounds'))
        const hashed_password = await bcrypt.hash(obj.password, salt)
        obj.password = hashed_password
        next()
    } catch (error) {
        next()
    }
    
}

// Custom functions Instance Method: new User().generateAuthToken()
userSchema.methods.generateAuthToken = function () {
    const token = jwt.sign({ _id: this._id, is_admin: this.is_admin }, config.get('Security.jwtPrivateKey'))
    return token
}

userSchema.statics.findByEmail = function (email, projection = '') {
    return this.findOne({email}, projection)
}

userSchema.methods.passwordMatches = function(password) {
    return bcrypt.compare(password, this.password)
}

const saveMiddleware = function (next) {
    const user = this
    password_encrypt(user, next)
}

userSchema.pre('save', saveMiddleware)

const User = mongoose.model('User', userSchema)

exports.User = User
exports.validate = validate
exports.validate_user_auth = validate_user_auth