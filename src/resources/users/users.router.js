const auth = require('../../security/auth.middleware')
const express = require('express')
const router = express.Router()

const user_controller = require('./users.controller')

router.get('/me', auth, user_controller.show)

module.exports = router;
