const { User } = require('./users.model')
const _ = require('lodash')

exports.show = async (req, res) => {
    const user = await User.findById(req.user._id).select('-password')
    res.send(user)
}