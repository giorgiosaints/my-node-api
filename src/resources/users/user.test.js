const { User } = require('./users.model')
const mongoose = require('mongoose')
const config = require('config')
const jwt = require('jsonwebtoken')

describe('method: user.generateAuthToken', () => {
    it('should return a valid JWT', () => {
        const payload = { 
            _id: new mongoose.Types.ObjectId().toHexString(), 
            is_admin: true 
        }
        const user = new User(payload)
        const token = user.generateAuthToken()
        const decoded = jwt.verify(token, config.get('Security.jwtPrivateKey'))

        expect(decoded).toMatchObject(payload)
    })
})