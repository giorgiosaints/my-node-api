const fs = require('fs')
const https = require('https')
const logger = require('./config/winston')
const config = require('config')
const express = require('express')
require('express-async-errors')
const app = express()

// const options = {
//     privateKey: fs.readFileSync(__dirname + config.get('Security.key')),
//     certificate: fs.readFileSync(__dirname + config.get('Security.certificate'))
// }

if (config.get('Log.enabled')) require('./startup/logger')(app)
require('./startup/routes')(app)
require('./startup/db')()

// // Startup server
// let server

// if (config.get('Security.enableHTTPS')) {
//     server = app.listen(config.get('Server.port'), () => {
//         logger.info(`Server is running o port: ${config.get('Server.port')}`)
//     })
// } else {
//     // Subindo com HTTPS
//     server = https.createServer(options, app).listen(config.get('Server.port'), () => {
//         logger.info(`Server is running o port: ${config.get('Server.port')}`)
//     })
    
// }

const server = app.listen(config.get('Server.port'), () => {
    logger.info(`Server is running o port: ${config.get('Server.port')}`)
})

module.exports = server