const morgan = require('morgan')
const logger = require('../config/winston')

module.exports = (app) => {
    app.use(morgan('combined', { stream: logger.stream }))
}