const mongoose = require('mongoose')
const logger = require('../config/winston')
const config = require('config')

module.exports = () => {
    // DB Connection
    // mongoose.connect(environment.db.url, { useNewUrlParser: true })
    mongoose.connect(config.get('DB.url'), { useNewUrlParser: true })
        .then(() => logger.info('Connected to MongoDB'))
}