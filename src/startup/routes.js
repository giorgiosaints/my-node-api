const express = require('express')
const body_parser = require('body-parser')
const rentals = require('../resources/rentals/rentals.router')
const plants = require('../resources/plants/plants.router')
const auth = require('../resources/auth/auth.router')
const users = require('../resources/users/users.router')
const error = require('../middlewares/error')

module.exports = (app) => {
    // Middlewares
    app.use(body_parser.json({ limit: '50mb' }));
    app.use(body_parser.urlencoded({ limit: '50mb', extended: true }));
    app.use(body_parser.json({ type: 'application/vnd.api+json' }));
    app.use(express.json())
    
    // Routes
    app.use('/api/users', users)
    app.use('/api/auth', auth)
    app.use('/api/plants', plants)
    app.use('/api/rentals', rentals)

    // Middlewares functions
    app.use(error)
}