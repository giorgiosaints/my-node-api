module.exports = {
  apps : [{
    name   : "udemy-node",
    script : "./src/server.js",
    instances: 0,
    exec_mode: "cluster",
    env: {
      NODE_CONFIG_DIR: "src/config"
    },
    env_production: {
      NODE_ENV: "production",
      NODE_CONFIG_DIR: "src/config"
    }
  }]
}
